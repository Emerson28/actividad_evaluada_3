
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
     
    </head>
    <body>
       
        <div class="container-actualizar ">
             <h1 align="center"class="text-light" >Actualizar Registro!</h1>
                    <form action="actualizar.do" method="POST">
                        <c:forEach var="listaTotal" items="${sessionScope.personas}">
                            <label class="dui form-label"><strong> DUI:</strong></label>
                            <input type="text" name="txtDui" value="${listaTotal.dui}" readonly class="input-dui form-control"/><br><br>

                            <label class="apellidos form-label"><strong>Apellidos:</strong></label>
                            <input type="text" name="txtApellidos" value="${listaTotal.apellidos}" class="input-apellidos form-control"/><br><br>

                            <label class="nombres form-label"><strong>Nombres:</strong></label>
                            <input type="text" name="txtNombres" value="${listaTotal.nombres}" class="input-nombres form-control"/><br><br>
                        </c:forEach>
                        <input type="submit" value="Actualizar Registro" class="btn btn-success border-2 border-dark form-control"/>
                    </form>
                    <form action="mostrar.do">
                        <input type="submit" class="btn btn-info border-2 border-dark form-control" value="Regresar"/>
                    </form>
  
        </div>
        
    </body>
   

</html>
