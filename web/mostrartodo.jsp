<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
        <title>JSP Page</title>
    </head>
    <body>
        
   <div class="container-mostrar mx-auto bg-warning border-dark shadow p-3 mb-5 rounded-3">
            <h1 class="text-dark" align="center">Todos los registros!</h1>
            <table class="table table-striped">
            <thead>
              <tr>
                <th class="text-dark border-dark border-2 bg-danger" scope="col">Dui</th>
                <th class="text-dark border-dark border-2 bg-danger" scope="col">Apellidos</th>
                <th class="text-dark border-dark border-2 bg-danger" scope="col">Nombres</th>
                <th class="text-dark border-dark border-2 bg-danger" scope="col">Eliminar</th>
                <th class="text-dark border-dark border-2 bg-danger" scope="col">Modificar</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="listaTotal" items="${sessionScope.personas}">
                <tr>
                    <td class="text-light bg-primary border-dark border-2"> ${listaTotal.dui}</td>
                    <td class="text-dark bg-light border-dark border-2">${listaTotal.apellidos}</td>
                    <td class="text-dark bg-info border-dark border-2">${listaTotal.nombres}</td>
                    <td class="bg-success border-2 border-dark">
                        <form action="borrar.do" method="post">
                            <input type="hidden" value="${listaTotal.dui}" name="txtEliminar">
                            <input  type="submit" class="btn btn-danger border-dark border-2" value="Eliminar">
                        </form>
                    </td>
                    
                    <td class="bg-success border-dark border-2" >
                        <form action="localizar.do" method="post">
                            <input  type="hidden" value="${listaTotal.dui}" name="txtEditar">
                            <input type="submit" class="btn btn-info border-dark border-2" value="Actualizar">
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
          </table>
                    <a class="btn btn-success" href="index.jsp">Menú Principal</a>

        </div>
    </body>
   

</html>