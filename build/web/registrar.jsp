

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       
     
    </head>
    <body>
        <div class="caja-registro bg-dark mx-auto shadow p-3 mb-5 rounded-3 border-5">
            <h1 class="fs-1 text-light position-relative" >Registro de personas</h1>
        
        <form action="recibir.do" method="POST">
            
            <label class="dui form-label"><strong> DUI:</strong></label><input type="text" name="txtDui" value="" class="dui form-control"/><br><br>
            <label class="apellidos form-label"><strong>Apellidos:</strong></label><input type="text" name="txtApellidos" value="" class="form-control apellidos"/><br><br>
            <label class="nombres form-label"><strong>Nombres:</strong></label><input type="text" name="txtNombres" value="" class="nombres form-control"/><br><br>
            <center>
                <input type="submit" value="Registrar Nueva Persona" class="btn btn-success"/><br><br>
                <a href="index.jsp" class="btn btn-warning"/>Menú Principal</a>
            </center>
        
        </form>
        </div>
    </body>
    

</html>
