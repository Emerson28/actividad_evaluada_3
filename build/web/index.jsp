

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>INICIO</title>
 
    </head>
    <body>
        
        <!--<div class="container">
            <div class="row">
                <div class="col">
                    
                </div>
                <div class="col-4">
                    <a href="registrar.jsp" class="btn btn-primary form-control">Registrar Nueva Persona</a>
                    <form action="mostrar.do">
                        <input type="submit" class="btn btn-primary form-control" value="Ver registros"/>
                    </form>
                </div>
                <div class="col">
                    
                </div>
            </div>
        </div>-->
        
        <div class="container py-4 mx-auto">

            <div class="row align-items-md-stretch">
              <div class="col-md-6">
                <div class="h-100 p-5 text-light bg-primary border border-5 border-dark rounded-3 ">
                    <h2 class="tt1">Registrar nueva persona</h2>
                  <p></p>
                  <a href="registrar.jsp" class="btn btn-dark box1 rounded-pill border-dark">Registrar</a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="h-100 p-5 bg-warning border-5 border-dark rounded-3">
                    <h2 class="tt2">Ver todos los registros</h2>
                  <p></p>
                    <form action="mostrar.do">
                        <input type="submit" class="btn btn-danger box2 rounded-pill border-dark" value="Ver registros"/>
                    </form>
                </div>
              </div>
            </div>
        </div>
    
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</html>